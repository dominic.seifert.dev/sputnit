import sputnit
import linux
import json::serialization

redef class SDLDisplay
	redef fun enable_mouse_motion_events do return true
end

redef class GamnitDisplay

	init
	do
		width = app.config.res_x
		height = app.config.res_y
	end
end

redef class App

	redef fun accept_event(event)
	do
		var camera_zoom_speed = 1.1
		if event isa SDLMouseButtonEvent then
			var key = event.button
			if key == 4 or key == 5 then
				var mod = if key == 4 then
						1.0/camera_zoom_speed
					else camera_zoom_speed
				world_camera.position.z *= mod
				world_camera.position.z = world_camera.position.z.clamp(60.0, 5000.0)
				return true
			end
		end

		return super
	end

	private var config_path: String = sys.program_name.dirname / "../client_config.json"

	private var config: ClientConfig do
		if config_path.file_exists then
			var content = config_path.to_path.read_all
			var deser = new ConfigDeserializer(content)
			var cc = deser.deserialize

			if cc == null then
				print_error "Error: Deserializing config file failed with {deser.errors.join(", ")}"
			else if not cc isa ClientConfig then
				print_error "Error: Deserializing config file failed, got '{cc}'"
			else return cc
		end

		# Save the default config to pretty Json
		var cc = new ClientConfig
		var json = cc.serialize_to_json(plain=true, pretty=true)
		json.write_to_file config_path

		return cc
	end
end

# Configuration of the client
class ClientConfig
	serialize

	# Resolution width
	var res_x = 1280 is lazy

	# Resolution height
	var res_y = 720 is lazy

	# Should the client play sounds?
	var play_sounds = true is lazy
end

redef class Sound
	redef fun play do if app.config.play_sounds then super
end

# `ClientConfig` deserializer
class ConfigDeserializer
	super JsonDeserializer

	# The only class we deserialize from pretty Json is ClientConfig
	redef fun class_name_heuristic(object) do return "ClientConfig"
end
