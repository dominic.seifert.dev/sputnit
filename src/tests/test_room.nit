module test_room is test_suite
import test_suite
import room

redef class Story
	redef var empty_room = new EmptyRoom(self)
	redef var lobby      = new Lobby(self)
end

class TestRoom
	super TestSuite

	fun test_slots
	do
		var g = new Game
		var station = g.station

		for i in [-10..10] do for j in [-10..10] do
			var slot = station.slot(i,j)
			assert slot.x == i
			assert slot.y == j
		end
	end

	fun test_room
	do
		var g = new Game
		var station = g.station

		var lobby = g.story.lobby

		for i in [-10..10] do for j in [-10..10] do
			var slot = station.slot(i,j)
			assert slot.room == null
			assert not lobby.try_spawn(slot).is_ok
		end

		var l1 = lobby.try_spawn(station.slot(0,0)).spawn
		var l2 = lobby.try_spawn(station.slot(0,1)).spawn
		var l3 = lobby.try_spawn(station.slot(2,0)).spawn
		assert not lobby.try_spawn(station.slot(0,0)).is_ok
		assert not lobby.try_spawn(station.slot(1,1)).is_ok

		l2.free_slots
		assert lobby.try_spawn(station.slot(1,1)).is_ok
	end
end
