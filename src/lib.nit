# General services
module lib

redef class RemovableCollection[E]
	# Remove each instance of `other` from `self`
	#
	# Don't confuse with `remove_all` which remove all instances of a single item.
	fun remove_each(other: Collection[E]) do for o in other do remove(o)
end

redef class Float
	# Fuzzy value in `[self-variation..self+variation]`
	fun &(variation: Float): Float do return self - variation + 2.0*variation.rand
end
