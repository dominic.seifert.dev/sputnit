module story is serialize

import game

redef class Story

	# Rooms
	redef var empty_room = new EmptyRoom(self,       cost=20)
	redef var lobby      = new Lobby(self,                     daily_cost= 10)
	redef var terminal   = new Terminal(self,                  daily_cost=100,
		power=20.0, oxygen=50.0)
	redef var cable      = new Cable(self)

	#                                        name        cost
	var stair      = new Stair(self,        "Staircase", cost= 50, daily_cost= 10, vspeed=0.4)
	var escalator  = new Stair(self,        "Escalator", cost=250, daily_cost= 25, vspeed=0.8,
		power=-0.5)
	var airlock    = new Airlock(self)

	var elevator   = new ElevatorRule(self, "Elevator",  cost=200,
		power=-2.0)

	#                                              name           size cost
	var research_lab = new RentableRoomRule(self, "Research lab", 4,1, 200, roles=[work]*4,             daily_rent=  20,
		power_per_roles=-1.0)
	var bar          = new RentableRoomRule(self, "Bar",          4,1, 400, roles=[work]*2+[leisure]*8, daily_rent=  20,
		power=-2.0)
	var sleep_pod    = new RentableRoomRule(self, "Sleep pod",    1,1,  50, roles=[residence]*1,        daily_rent=   5)
	var condominium  = new BuyableRoomRule(self,  "Condominium",  3,1, 100, roles=[residence]*2,     one_time_cost= 200)
	var dock         = new BuyableRoomRule(self,  "Dock",         5,3, 500, roles=[work]*6,          one_time_cost=1000,
		power=-10.0)

	var solar        = new RoomRule(self,         "Solar",        4,1, 200, structural=false,
		power=10.0)
	var reactor      = new RoomRule(self,         "Reactor",      4,3,5000, roles=[work]*4,
		power_per_roles=50.0)

	var o2_generator = new RoomRule(self,         "O2 Generator", 4,1, 250, roles=[work]*4,
		power_per_roles=10.0, oxygen_per_roles=10.0)
	var greenhouse = new RoomRule(self,           "Greenhouse",   5,2, 400, roles=[work]*2,
		                      oxygen_per_roles=10.0)

	# Techs
	var start     = new Tech(self, "Start",           new Array[TechGoal])
	var bootstrap = new Tech(self, "Basic crew",      [new PopGoal( 25)],
		unlocks=[condominium, dock, greenhouse])
	var spaceport = new Tech(self, "Spaceport",       [new PopGoal(100), new RoomGoal(dock, 1): TechGoal],
		unlocks=[escalator, o2_generator])
	var resources = new Tech(self, "Space resources", [new PopGoal(200)],
		unlocks=[reactor])
	var hub       = new Tech(self, "Trade hub",       [new PopGoal(500)])
end
