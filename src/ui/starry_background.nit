import background_sprites
import station_view

redef class App

	private var texture_stars = new Texture("textures/stars.png")
	private var texture_stars_day = new Texture("textures/stars-day.png")
	private var texture_sun = new Texture("textures/lens_flare/sparkle.png")
	private var texture_flare = new Texture("textures/lens_flare/hexangle.png")

	private var stars = new Sprite(texture_stars, new Point3d[Float](0.0, -0.33, -1.1)) is lazy
	private var stars_day = new Sprite(texture_stars_day, new Point3d[Float](0.0, -0.33, -1.1001)) is lazy
	private var sun = new Sprite(texture_sun, new Point3d[Float](0.0, -0.33, -1.0)) is lazy
	private var flares: Array[Sprite] =
		[for i in 3.times do new Sprite(texture_flare, new Point3d[Float](0.0, 0.0, -0.9))] is lazy

	redef fun on_create
	do
		super

		background_sprites.add stars_day
		stars_day.scale = 4.0 / texture_stars.height

		background_sprites.add stars
		stars.scale = 4.0 / texture_stars.height

		background_sprites.add sun
		sun.scale = 1.0/192.0

		app.background_sprites.add_all flares
		flares[0].scale = 1.0 / 1024.0
		flares[0].alpha = 0.3
		flares[1].scale = 1.0 / 750.0
		flares[1].alpha = 0.12
		flares[2].scale = 1.0 / 1532.0
		flares[2].alpha = 0.18
	end

	redef fun update(dt)
	do
		super

		if not paused then
			var tod = game.time_of_day
			var a = tod * 2.0 * pi - 0.5 * pi
			stars.rotation = a
			stars_day.rotation = a

			var c
			if tod > 0.1 and tod < 0.9 then
				var p_to_noon = (0.5-tod).abs
				c = p_to_noon/0.4
				c = 1.1 - (1.0-c).sqrt
				if c.is_nan then c = 1.0
				c = c.clamp(0.2, 1.0)
			else c = 1.0
			stars.alpha = c

			var d = 1.2
			sun.center.x = stars.center.x + d*a.cos
			sun.center.y = stars.center.y - 0.2 + d*a.sin

			# TODO hide flares when the sun is out of the screen
			for f in flares, p in [0.66, 0.54, 0.38] do
				f.center.x = sun.center.x * p
				f.center.y = sun.center.y * p
			end
		end
	end
end
