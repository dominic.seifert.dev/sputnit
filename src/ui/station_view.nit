import gamnit::flat
import gamnit::depth
import gamnit::keys
intrude import gamnit::tileset
import app::audio
import binary::serialization

import game
import lib

import characters_spritesheet

redef class App

	var game = new Game
	var save_path = "save.sputnit"


	var buttons = new Array[Button]

	# TODO refactor into selected action or something
	var selected_room_rule: nullable RoomRule = null
	var selected_free = false

	private var texture_bulldozer = new Texture("textures/bulldozer.png")
	var texture_warning = new Texture("textures/warning.png")
	var texture_checkmark = new Texture("textures/checkmark.png")
	var texture_cross = new Texture("textures/cross.png")
	var texture_star = new Texture("textures/star.png")
	var texture_bolt = new Texture("textures/bolt.png")
	var texture_o2 = new Texture("textures/O2.png")

	private var texture_space_car = new Texture("textures/space_car.png")
	private var texture_elevator_car = new Texture("textures/elevator_car.png")

	# Sprites half-transparent shown when overing
	var over_sprites = new Array[Sprite]

	var characters_spritesheet = new Characters_SpritesheetImages

	var texture_font = new Texture("fonts/berlin.png")
	var font = new TileSetFont(texture_font, 32, 32, " "*32 + """
 !"#$%&'()*+,-./
0123456789:;<=>?
@ABCDEFGHIJKLMNO
PQRSTUVWXYZ[\\]^_
`abcdefghijklmno
pqrstuvwxyz{|}~ """ + " "*(8*16))


	var sprites_status = new TextSprites(font, ui_camera.top_left.offset(16, -16, 0)) is lazy

	# General info display
	var sprites_describe = new TextSprites(font, ui_camera.top_right.offset(-16, -16, 0)) is lazy

	var sprites_goals = new TextSprites(font, ui_camera.top_left.offset(24.0, -92, 0)) is lazy

	var tech_level_stars = new Array[Sprite]
	var power_rating_bolts = new Array[Sprite]

	var icon_o2 = new Sprite(texture_o2, ui_camera.top_left.offset(364, -64, 0)) is lazy
	var sprites_o2 = new TextSprites(font, ui_camera.top_left.offset(400, -56, 0)) is lazy

	# Is the game on pause?
	var paused = false

	# Game speed
	var game_speed = 1.0

	redef fun on_create
	do
		# Force register room textures
		for rule in game.story.rules do if rule isa RoomRule then rule.texture

		super

		# TODO splash screen

		for model in models do model.load
		font.hspace = -16
		font.vspace = -4

		# Insert extra glyphs
		font.chars = font.chars_cleaned.to_s + "✓✘★"
		font.subtextures.add texture_checkmark
		font.subtextures.add texture_cross
		font.subtextures.add texture_star

		world_camera.reset_height 1080.0

		game.setup

		#var a = new Actor(placeholder_model, new Point3d[Float](0.0, 0.0, 0.0))
		#actors.add a

		var x = 32.0
		var y = 64.0
		var scale = 0.75

		var s: Button = new FreeSlotButton(texture_bulldozer,
			ui_camera.bottom_left.offset(x, y, 0.0))
		s.scale = scale
		ui_sprites.add s
		buttons.add s

		# Rooms button
		for tech in [null: nullable Tech] + game.story.techs do
			for rule in game.story.rules do if rule isa RoomRule and not rule.static and rule.tech == tech then
				x += 64.0
				if buttons.length % 2 == 0 then
					y += (92.0 + 12.0)*scale
					x = 32.0
				end

				s = new RoomRuleButton(rule.texture, ui_camera.bottom_left.offset(x, y, 0.0), rule)
				if rule.tech != null then s.alpha *= 0.25
				s.scale = scale
				ui_sprites.add s
				buttons.add s
			end
		end

		# Power
		for i in 8.times do
			var bolt = new Sprite(texture_bolt, ui_camera.top_left.offset(200.0 + i.to_f*16.0, -64.0, 0.0))
			bolt.scale = 0.7
			bolt.tint[0] = 0.0
			bolt.tint[1] = 0.0
			bolt.tint[2] = 0.0
			ui_sprites.add bolt
			power_rating_bolts.add bolt
		end

		# Tech level
		for i in game.story.techs.length.times do
			var star = new Sprite(texture_star, ui_camera.top_left.offset(24.0 + i.to_f*32.0, -64.0, 0.0))
			star.tint[0] = 0.0
			star.tint[1] = 0.0
			star.tint[2] = 0.0
			ui_sprites.add star
			tech_level_stars.add star
		end

		# O2
		ui_sprites.add icon_o2
		icon_o2.scale = 0.7
	end

	redef fun update(dt)
	do
		super

		if not paused then
			var turn = game.update(dt*game_speed)

			# Show transactions of the last day
			if turn.new_day then
				# Categorize transactions
				var history = game.station.cash.history
				var categorised = new DefaultMap[String, Int](0)
				var total = 0
				for h in history do
					#var from = h.from
					#if from == null then
						#from = "Other"
					#else from = from.to_s

					var reason = h.reason
					if reason == null then
						reason = "Other"
					else reason = reason.to_s
					categorised[reason] += h.amount
					total += h.amount
				end
				history.clear

				# Form text
				var total_label = "Daily total"
				var first_column_width = total_label.to_s.length
				for k in categorised.keys do first_column_width = first_column_width.max(k.to_s.length)

				var lines = new Array[String]
				for k, v in categorised do
					lines.add "{k.justify(first_column_width, 1.0)} ${v}"
				end
				alpha_comparator.sort lines
				lines.add(" "*(first_column_width-1) + "---")
				lines.add "{total_label.justify(first_column_width, 1.0)} ${total}"

				# Draw
				var text = lines.join("\n")
				app.sprites_describe.anchor.x = app.ui_camera.top_right.x - 16.0 - app.font.text_width(text).to_f
				sprites_describe.text = text
			end
		end

		var camera_scroll_speed = 2000.0*dt
		for key in app.pressed_keys do
			if key == "w" or key == "up" then
				world_camera.position.y += camera_scroll_speed
			else if key == "s" or key == "down" then
				world_camera.position.y -= camera_scroll_speed
			else if key == "a" or key == "left" then
				world_camera.position.x -= camera_scroll_speed
			else if key == "d" or key == "right" then
				world_camera.position.x += camera_scroll_speed
			end
		end
		world_camera.position.x = world_camera.position.x.clamp(-2000.0, 2000.0)
		world_camera.position.y = world_camera.position.y.clamp(0.0, 4000.0)

		# Status
		sprites_status.text = "{game.station.cash.to_s_smooth}, {game.station.people.length} pop, {(game.station.happiness*100.0).round.to_i}% happiness, {game.station.power} power, {game.station.oxygen} O2"

		# Tech level
		sprites_goals.text = game.station.goals_text

		var s = 0
		var tech_level = game.station.tech_level.order
		for star in tech_level_stars do
			if s <= tech_level then
				star.tint[0] = 1.0
				star.tint[1] = 1.0
			else
				star.tint[0] = 0.0
				star.tint[1] = 0.0
			end
			s += 1
		end

		# Power
		s = 0
		var pr = game.station.power.rating.units_smooth
		var color = game.station.power.rating.color(pr)
		for bolt in power_rating_bolts do
			if (pr*4.0+4.0).to_i <= s then
				for i in 3.times do bolt.tint[i] = 0.0
			else
				bolt.tint.clear
				bolt.tint.add_all color
			end
			s += 1
		end

		# O2
		var d = (game.station.oxygen.supply.units - game.station.oxygen.demand.units).round.to_i
		sprites_o2.text = if d > 0 then "+" + d.to_s else d.to_s
		color = game.station.oxygen.rating.color
		for i in 3.times do icon_o2.tint[i] = color[i]
	end

	redef fun accept_event(event)
	do
		super

		if event isa KeyEvent and event.name == "escape" then
			exit 0
		else if event isa KeyEvent and event.is_down then
			var key = event.name
			if key == "space" then
				paused = not paused
			else if key == "." then
				game_speed *= 2.0
				game_speed = game_speed.clamp(1.0, 64.0)
			else if key == "," then
				game_speed /= 2.0
				game_speed = game_speed.clamp(1.0, 64.0)
			else if key == "c" then
				game.story.worker.spawn(game.station, game.station.slot(0, 0))
			else if key == "k" then
				save_game
			else if key == "l" then
				load_game
			end
		else if event isa PointerEvent and (event.pressed or event.is_move) then

			for s in over_sprites do sprites.remove s
			over_sprites.clear

			var p = world_camera.camera_to_world(event.x, event.y)

			var rule = selected_room_rule
			if rule != null then
				# Get the bottom-left slot of the centered room
				var x = (p.x/64.0 - (rule.width - 1).to_f / 2.0).round.to_i
				var y = (p.y/92.0 - (rule.height - 1).to_f / 2.0).round.to_i
				var lot = game.station.slot(x, y)
				var can = rule.try_spawn(lot)

				for slot in can.slots do
					var s = new Sprite(rule.texture,
						new Point3d[Float](slot.screen_x, slot.screen_y, 0.0))
					s.alpha = 0.5
					if not can.is_ok then
						s.tint[1] = 0.5
						s.tint[2] = 0.5
					end
					app.sprites.add s
					over_sprites.add s
				end
				if can.is_ok and rule.walkable and not can.reachable_from_terminal then
					var s = new Sprite(app.texture_warning,
						new Point3d[Float](lot.screen_x, lot.screen_y, 1.0))
					s.tint[1] = 0.0
					s.tint[2] = 0.0
					s.scale = 0.5

					app.sprites.add s
					over_sprites.add s
				end
				return true
			end

			var x = (p.x/64.0).round.to_i
			var y = (p.y/92.0).round.to_i
			var lot = game.station.slot(x, y)
			var room = lot.room
			if selected_free and room != null then
				var can = room.can_free
				for l in room.slots do
					var s = new Sprite(room.rule.texture,
						new Point3d[Float](l.screen_x, l.screen_y, 0.0))
					s.tint[1] = 0.5
					s.tint[2] = 0.5
					if not can then s.tint[0] = 0.5
					app.sprites.add s
					over_sprites.add s
				end
			end
		else if event isa PointerEvent and event.depressed and not event.is_move then
			for s in over_sprites do sprites.remove s
			over_sprites.clear

			for but in buttons do
				if but.hit(event) then return true
			end

			var p = world_camera.camera_to_world(event.x, event.y)

			var rule = selected_room_rule
			if rule != null then
				# Get the bottom-left slot of the centered room
				var x = (p.x/64.0 - (rule.width - 1).to_f / 2.0).round.to_i
				var y = (p.y/92.0 - (rule.height - 1).to_f / 2.0).round.to_i
				var lot = game.station.slot(x, y)

				# Add an elevator car?
				var room = lot.room
				if room isa Elevator and room.rule == rule and room.can_buy_car then
					room.buy_car lot
					return true
				end

				var can = rule.try_spawn(lot)
				if can.is_ok then can.spawn
				return true
			end

			var x = (p.x/64.0).round.to_i
			var y = (p.y/92.0).round.to_i
			var lot = game.station.slot(x, y)
			if selected_free then
				var room = lot.room
				if room != null and room.can_free then
					room.free_slots
				end
			else
				lot.dump

				# Show path between last and current slot
				var last_slot = self.last_slot
				if last_slot != null then
					var path = last_slot.path_to(lot)
					if path != null then
						print "path from {last_slot} to {lot}: {path.plan}"
					else
						print "not path from {last_slot} to {lot}"
					end
				end
				self.last_slot = lot
			end

			#sprites.first.center.x = p.x
			#sprites.first.center.y = p.y
			#sprites.first.tint[2] = 1.0.rand
			#sprites.first.tint[1] = 1.0.rand
		else if event isa QuitEvent then
			exit 0
		end

		return false
	end

	# The last slot clicked in free mode
	var last_slot: nullable Slot = null

	fun save_game
	do
		#game.serialize_to_json(pretty=true, plain=true)
		var stream = new FileWriter.open(save_path)
		var engine = new BinarySerializer(stream)
		engine.serialize game
		stream.close

		feedback "Game saved to '{save_path}'"
	end

	fun load_game
	do
		var stream = new FileReader.open(save_path)
		var engine = new BinaryDeserializer(stream)
		var game = engine.deserialize
		stream.close

		if engine.errors.not_empty then
			feedback "Deserialization errors:\n" + engine.errors.join("\n")
		else if game == null then
			feedback "Deserialization error: got `null`"
		else if not game isa Game then
			feedback "Deserialization error: got a `{game.class_name}`"
		else
			# Set as current game
			self.game = game

			# Update display
			sprites.clear
			for c in game.station.people do c.display
			for r in game.station.rooms do r.display

			# Load missing textures
			# FIXME reuse previously loaded textures
			for rule in game.story.rules do if rule isa RoomRule then rule.texture
			for texture in all_root_textures do texture.load

			# Notify user
			feedback "Game loaded"
		end
	end

	fun feedback(text: Text)
	do
		app.sprites_describe.anchor.x = app.ui_camera.top_right.x - 16.0 - app.font.text_width(text).to_f
		app.sprites_describe.text = text
		print text
	end
end

redef class Station
	redef fun tech_level=(lvl)
	do
		super

		for but in app.buttons do
			if but isa RoomRuleButton and but.room_rule.tech == lvl then
				but.alpha /= 0.25
			end
		end
	end
end

redef class SpaceElevator
	var car_sprite: Sprite is lazy do
		var s = new Sprite(app.texture_space_car, new Point3d[Float](0.0, 0.0, 0.5))
		app.sprites.add s
		return s
	end

	redef fun update(turn)
	do
		super

		var y = -0.13 * 92.0
		if next_event_is_arrival then
			var time_left = next_event_time - turn.time
			var p = time_left / (period_out * turn.game.day_length) - 0.5
			p = (0.5 - p.abs)*2.0

			car_sprite.center.y = -p * 2000.0 + y
		else car_sprite.center.y = y
	end
end

redef class ElevatorCar
	var car_sprite: Sprite is lazy do
		var s = new Sprite(app.texture_elevator_car,
			new Point3d[Float](elevator.slots.first.screen_x, elevator.slots.first.screen_y, 0.5))
		app.sprites.add s

		var n = 0
		for c in elevator.cars do
			if c == self then break
			n += 1
		end

		var c = 1.0 - n.to_f * 0.2
		for i in 3.times do s.tint[i] = c
		return s
	end

	redef fun update(turn)
	do
		super

		car_sprite.center.y = y.to_f * 92.0 - 16.0
	end
end

redef class Elevator
	redef fun on_join_lot(lot)
	do
		var olds = lot.sprite
		if olds != null then app.sprites.remove olds
		var s = new Sprite(rule.texture,
			new Point3d[Float](lot.screen_x, lot.screen_y, 0.0))
		lot.sprite = s
		app.sprites.unshift s
	end

	redef fun free_slots
	do
		super

		for car in cars do
			app.sprites.remove car.car_sprite
		end
	end
end

class Button
	super Sprite

	fun hit(event: PointerEvent): Bool
	do
		var y = -event.y
		var dx = (event.x - center.x).abs
		var dy = (y - center.y).abs

		var r = dx < 32.0 and dy < 48.0
		if r then act
		return r
	end

	fun act do print "Action!"
end

class RoomRuleButton
	super Button

	var room_rule: RoomRule

	redef fun act
	do
		if app.selected_free then app.buttons.first.tint[2] /= 0.25
		for but in app.buttons do
			if but isa RoomRuleButton and but.room_rule == app.selected_room_rule then
				but.tint[2] /= 0.25
			end
		end
		app.selected_free = false

		if app.selected_room_rule == room_rule then
			app.selected_room_rule = null
			app.sprites_describe.text = null
			return
		end

		app.selected_room_rule = room_rule

		var desc = room_rule.describe
		app.sprites_describe.anchor.x = app.ui_camera.top_right.x - 16.0 - app.font.text_width(desc).to_f
		app.sprites_describe.text = desc
		tint[2] *= 0.25
	end
end

class FreeSlotButton
	super Button

	init do scale = 64.0 / texture.width

	redef fun act
	do
		if app.selected_free then app.buttons.first.tint[2] /= 0.25
		for but in app.buttons do
			if but isa RoomRuleButton and but.room_rule == app.selected_room_rule then
				but.tint[2] /= 0.25
			end
		end
		app.selected_room_rule = null

		app.selected_free = not app.selected_free
		if app.selected_free then tint[2] *= 0.25
	end
end

redef class RoomRule

	var texture = new Texture("textures/{name.to_s.replace(" ", "").to_lower}.png") is lazy

	# Tint when in use
	var use_tint: Array[Float] is lazy do
		if roles.count(story.leisure) > 0 then return [0.5, 0.5, 1.0, 1.0]
		if roles.count(story.work) > 0 then return [0.2, 0.5, 1.0, 1.0]
		if roles.count(story.residence) > 0 then return [0.5, 0.8, 0.5, 1.0]
		return [1.0, 1.0, 1.0, 1.0]
	end
end

redef class Room
	redef fun on_spawn
	do
		super
		display
	end

	fun display
	do
		print "room::display {slots}"
		for lot in slots do
			var olds = lot.sprite
			if olds != null then app.sprites.remove olds
			var s = new Sprite(
				rule.texture,
				new Point3d[Float](lot.screen_x, lot.screen_y, 0.0))
			lot.sprite = s
			app.sprites.unshift s
		end
	end

	redef fun on_free
	do
		super
		for lot in slots do
			var s = lot.sprite
			assert s != null
			app.sprites.remove s
		end
	end

	fun screen_x: Float
	do
		var sum = 0.0
		for lot in slots do sum += lot.screen_x
		return sum / slots.length.to_f
	end

	fun screen_y: Float
	do
		var sum = 0.0
		for lot in slots do sum += lot.screen_y
		return sum / slots.length.to_f
	end
end

redef class Slot
	var sprite: nullable Sprite = null

	fun screen_x: Float do return x.to_f * 64.0
	fun screen_y: Float do return y.to_f * 92.0
end

redef class Character
	var sprite = new Sprite(app.characters_spritesheet.characters.rand.standing,
		new Point3d[Float](x, y, 10.0))

	init do display

	fun display
	do
		sprite.scale = 0.3
		app.sprites.add sprite
	end

	redef fun leave_station(turn)
	do
		super
		app.sprites.remove sprite
	end

	redef fun x=(x)
	do
		super
		sprite.center.x = x.to_f * 64.0

		var dy = y - y.floor
		if dy != 0.0 then
			# Stairs
			var w = 40.0
			var dx
			if dy < 0.66 then
				dx = -w*0.5 + dy * w / 0.66
			else dx = w*0.5 - (dy-0.66) * w / 0.33
			sprite.center.x += dx
		end
	end

	redef fun y=(y)
	do
		super
		sprite.center.y = y.to_f * 92.0 - 15.0
	end
end

redef class Characters_SpritesheetImages
	var characters = new Array[CharacterSpritesheet].with_items(
		new CharacterSpritesheet(green_standing),
		new CharacterSpritesheet(blue_standing),
		new CharacterSpritesheet(pink_standing),
		new CharacterSpritesheet(yellow_standing))
		#new CharacterSpritesheet(gray_standing)) it doesn't fit the current style
end

class CharacterSpritesheet
	var standing: Texture
	#var walk: Array[Texture] TODO
end

redef class BuyableRoom
	redef fun display
	do
		super
		if bought then display_bought
	end

	redef fun buy(turn)
	do
		super
		display_bought
	end

	fun display_bought
	do
		var tint = rule.use_tint
		for lot in slots do
			var s = lot.sprite
			assert s != null
			for i in 3.times do s.tint[i] *= tint[i]
		end
	end
end

redef class RentableRoom

	redef fun display
	do
		super
		if rented then display_rented
	end

	redef fun start_renting(turn)
	do
		super
		display_rented
	end

	fun display_rented
	do
		var tint = rule.use_tint
		for lot in slots do
			var s = lot.sprite
			assert s != null
			for i in 3.times do s.tint[i] *= tint[i]
		end
	end

	redef fun stop_renting
	do
		super
		var tint = rule.use_tint
		for lot in slots do
			var s = lot.sprite
			assert s != null
			for i in 3.times do s.tint[i] /= tint[i]
		end
	end
end

redef class FluidRating
	fun color(rating: nullable Float): Array[Float]
	do
		rating = rating or else units_smooth
		return if rating > 0.5 then
			[0.0, 1.0, 0.0, 1.0] # green
		else if rating >= 0.0 then
			[1.0, 1.0, 0.0, 1.0] # yellow
		else [1.0, 0.0, 0.0, 1.0] # red
	end
end
