intrude import gamnit::flat
import gamnit::depth

redef class App
	# Sprite being drawn at the back of the screen
	#
	# Not affected by `world_camera` position.
	var background_sprites: Sequence[Sprite] = new List[Sprite]

	# Draw the sprites as 3 layers, clearing the depth buffer after each one:
	# 1. `background_sprites`
	# 2. world `sprites`
	# 3. `ui_sprites`
	redef fun frame_core_draw(display)
	do
		frame_core_background_sprites display
		super
	end

	fun frame_core_background_sprites(display: GamnitDisplay)
	do
		simple_2d_program.use

		simple_2d_program.coord.array_enabled = true
		simple_2d_program.tex_coord.array_enabled = true
		simple_2d_program.color.array_enabled = false

		simple_2d_program.mvp.uniform world_camera.mvp_matrix_no_translation
		for sprite in background_sprites do sprite.draw

		glClear gl_DEPTH_BUFFER_BIT
	end
end

redef class EulerCamera

	# Projection and rotation matrix but without translation
	protected fun mvp_matrix_no_translation: Matrix
	do
		var view = rotation_matrix
		var projection = new Matrix.perspective(pi*field_of_view_y/2.0,
			display.aspect_ratio, near, far)

		return view * projection
	end
end
