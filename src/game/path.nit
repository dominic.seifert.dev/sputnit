module path is serialize

import room
private import ai::search

# A path solution from a path finding problem
class PathPlan
	var plan: Sequence[Slot]
end

redef class Slot
	# Find a path from self to a given destination
	fun path_to(destination: Slot): nullable PathPlan
	do
		var pp = new PathProblem(self, destination)
		var sol = pp.astar.run
		if sol == null then return null
		var res = new PathPlan(sol.plan)
		return res
	end
end

private class PathProblem
	super SearchProblem[Slot, Slot]

	redef var initial_state
	var goal: Slot
	redef fun is_goal(s) do return s == goal

	redef fun actions(s) do return s.walk.to_a
	redef fun apply_action(s, a) do return a
	redef fun heuristic(s) do return (s.x.distance(goal.x) + s.y.distance(goal.y)).to_f
end

redef class Room
	# Is this room reachable from the terminal
	fun reachable_from_terminal: Bool
	do return slots.first.path_to(station.slot(0, 0)) != null
end

redef class RoomAttempt
	var reachable_from_terminal: Bool is lazy do
		return rule.reachable_from_terminal(self)
	end
end

redef class RoomRule
	# I do not like this method since is duplicates the rules of `connect_walk`
	# TODO find a better approach
	fun reachable_from_terminal(room: RoomAttempt): Bool
	do
		var zero = room.station.slot(0, 0)
		var slot = room.anchor
		if slot.left.path_to(zero) != null then return true
		if slot.move(width, 0).path_to(zero) != null then return true
		return false
	end
end

redef class Stair
	redef fun reachable_from_terminal(room)
	do
		if super then return true
		var zero = room.station.slot(0, 0)
		var slot = room.anchor
		var x = slot.up
		if x.rule == self and x.path_to(zero) != null then return true
		x = slot.down
		if x.rule == self and x.path_to(zero) != null then return true
		return false
	end
end
