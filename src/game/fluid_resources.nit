module fluid_resources is serialize

import serialization

class FluidResource

	var supply = new FluidCount

	var demand = new FluidCount

	# Simplified state of the power in `[-1.0..1.0]` where under 0.0 means lack of power
	var rating = new FluidRating(self)

	redef fun to_s do return "{demand}/{supply}"
end

class FluidCount
	var units = 0.0 is writable

	# Attenuated over time version of `units` for the UI
	#
	# We don't really care about dt at this point, so each call acts as a step.
	fun units_smooth: Float
	do
		var s = 16.0
		var d = units.to_f - units_last

		var u
		if d.abs < 0.1 then
			u = units
		else u = units_last + d/s
		units_last = u

		return u
	end

	private var units_last: Float = units is lazy

	redef fun to_s do return units_smooth.to_precision(0)
end

class FluidRating
	super FluidCount

	var resource: FluidResource

	redef fun units
	do
		var range = resource.demand.units/5.0
		range = range.clamp(10.0, 100.0)

		var d = resource.supply.units - resource.demand.units
		d = d / range
		return d.clamp(-1.0, 1.0)
	end
end
