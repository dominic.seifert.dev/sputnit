module tech is serialize

import base
import characters
import room

redef class Station

	var tech_level: Tech is noinit

	var techs = new Array[Tech]

	var goals_text = ""

	redef fun update(turn)
	do
		super

		check_to_update_rating(turn)
	end

	# Check if the rating is to be upgraded
	private fun check_to_update_rating(turn: Turn)
	do
		var rating = tech_level.order
		var text_lines = new Array[String]
		loop
			text_lines.clear

			if rating >= turn.game.story.techs.length - 1 then
				text_lines.add "No more tech levels"
				break
			end

			text_lines.add "Goals for the next tech level:"

			var next_tech = tech_level.story.techs[rating+1]
			var pass = true
			for goal in next_tech.goals do
				var mark
				if goal.is_ok(self) then
					mark = "✓"
				else
					mark = "✘"
					pass = false
				end
				text_lines.add "{mark} {goal.describe}"
			end

			if not pass then break

			rating += 1
			tech_level = turn.game.story.techs[rating]
			techs.add tech_level
		end

		goals_text = text_lines.join("\n")
	end
end

redef class Game
	redef fun setup
	do
		super
		station.tech_level = story.techs.first
	end
end

redef class Story
	var techs = new Array[Tech]
end

redef class RoomRule
	# Tech needed to build this room
	var tech: nullable Tech = null is optional
end

class Tech
	super Rule

	var goals: Array[TechGoal]

	var unlocks = new Array[RoomRule] is optional

	init do
		story.techs.add self
		for room in unlocks do room.tech = self
	end

	fun order: Int do return story.techs.index_of(self)
end

class TechGoal
	fun is_ok(station: Station): Bool is abstract

	fun describe: String is abstract
end

class PopGoal
	super TechGoal

	var min_pop: Int
	redef fun is_ok(s) do return s.people.length >= min_pop
	redef fun describe do return "Population of {min_pop}"
end

class RoomGoal
	super TechGoal

	var rule: RoomRule
	var count: Int

	redef fun is_ok(s) do return [for r in s.rooms do if r.rule == rule then r].length >= count
	redef fun describe do return "Build at least {count} {rule.name}"
end

redef class RoomAttempt
	redef fun is_ok
	do
		if not super then return false

		var tech = rule.tech
		return tech == null or station.techs.has(tech)
	end
end
