module characters is serialize

import more_collections

import lib

import base
import room
import resource
import path

abstract class CharacterRule
	super Rule

	var rule_count = 0

	fun spawn(station: Station, slot: Slot, name: nullable Text) do
		if name == null then name = "{self.name}_{rule_count}"
		rule_count += 1
		var c = new Character(self, name, slot, station)
		station.people.add c
	end
end

class Builder
	super CharacterRule
end

class Janitor
	super CharacterRule
end

class Guard
	super CharacterRule
end

class Nurse
	super CharacterRule
end

class Worker
	super CharacterRule
end

redef class Story
	var builder = new Worker(self)
	var janitor = new Janitor(self)
	var guard = new Guard(self)
	var nurse = new Nurse(self)
	var worker = new Worker(self)
end

class Character
	super Ruled
	super Turnable

	redef type RULE: CharacterRule

	var name: Text
	var current: nullable Job = null
	var money = 0
	var energy = 100.0

	var speed: Float = 2.2 + 0.4.rand
	var current_plan: PathPlan is noinit

	var current_slot: Slot

	var x: Float is noinit
	var y: Float is noinit

	var station: Station

	var tendency: Float = 0.0 & 0.05

	# Roles and rooms to which `self` is assigned
	var roles = new DefaultMap[Role, nullable Room](null)

	# Assign `self` to `room` as `role`
	fun take_role(turn: Turn, role: Role, room: Room)
	do
		roles[role] = room
		room.assigned_roles[role].add self
		room.available_roles[role] -= 1
	end

	# Unassign from `role` whatever was the room
	fun leave_role(role: Role)
	do
		var room = roles[role]
		if room == null then return

		roles[role] = null
		room.assigned_roles[role].remove self
		room.available_roles[role] += 1
	end

	# Reconsider roles to see if the rooms are still accessible
	#
	# Returns true if a role was dropped
	fun reconsider_roles: Bool
	do
		for role, room in roles do
			if room != null and not room.reachable_from_terminal then
				leave_role role
				return true
			end
		end
		return false
	end

	init do
		x = current_slot.x.to_f
		y = current_slot.y.to_f
	end

	redef fun update(turn) do
		var curr = current
		if curr == null then
			curr = station.pick_job(self)
			if curr != null then
				current_plan = current_slot.path_to(curr.slot).as(not null)
				current = curr
			end
		end

		# If no real job could be found, just have `self` walk randomly
		if curr == null then
			var sl = current_slot

			var target = where_to_go(turn)
			if target == sl.room and roles[turn.game.story.residence] == target then
				# If already at home, don't move around
				# TODO use a real job
				return
			end

			var next = target.slots.rand
			while next.y != target.slots.first.y do next = target.slots.rand

			var path = sl.path_to(next)
			if path == null then
				# Can't go to destination, possible causes:
				# * Home, work or leisure may be unaccessible -> reconsider_roles
				# * Is in an isolated part of the station -> panic / go to a random acessible room?
				# * May be in outer space / destroyed room -> die
				var found_prob = reconsider_roles
				if not found_prob then die turn
				return
			end

			curr = new WalkJob.with_slot(next)
			current_plan = path
			current = curr
			#print "New plan for {self}. Go to {next}"
			#print "Path:"
			#print current_plan.plan
		end
		var pl = current_plan
		if pl.plan.is_empty then
			#print "Performing job"
			curr.update(turn)
		else
			var next = pl.plan.first

			# Check if is still usable
			var next_room = next.room
			if next_room == null or not next_room.rule.walkable then
				current = null
				return
			end
			var sl = current_slot
			var x_dir = -1.0
			if sl.x < next.x then
				x_dir = 1.0
			else if sl.x == next.x then
				x_dir = 0.0
			end
			var y_dir = -1.0
			if sl.y < next.y then
				y_dir = 1.0
			else if sl.y == next.y then
				y_dir = 0.0
			end
			#print "Direction: x = {x_dir}, y = {y_dir}"
			#print "Next is ({next.x},{next.y}) | Current slot is ({sl.x},{sl.y})"

			# Stairs are slow
			var mod = 1.0
			var current_slot_rule = current_slot.rule
			if current_slot_rule isa Stair then mod = current_slot_rule.vspeed

			x += x_dir * (turn.dt * speed)
			y += y_dir * (turn.dt * speed) * mod

			# Clamp x and y on the goal
			var goal_x = next.x.to_f
			var goal_y = next.y.to_f
			if x * x_dir > goal_x * x_dir then x = goal_x
			if y * y_dir > goal_y * y_dir then y = goal_y
			if x == goal_x and y == goal_y then
				current_slot = pl.plan.shift
				#print "Updated plan, remaining destinations: {pl.plan}"
			end
		end
		if curr.is_finished then current = null
	end

	# Leave the station, for a short while or longer
	fun leave_station(turn: Turn)
	do
		station.people.remove self

		for role in roles.keys.to_a do leave_role role
	end

	# Die and don't come back, calls `leave_station`
	fun die(turn: Turn)
	do
		leave_station turn

		# TODO delete all references
	end

	# Where should `self` go at this `turn.time_of_da`y?
	fun where_to_go(turn: Turn): Room
	do
		var tod = turn.game.time_of_day

		var work = turn.game.story.work
		var residence = turn.game.story.residence
		var leisure = turn.game.story.leisure

		var target = null
		if tod < 0.3 + tendency then
			target = roles[residence]
		else if tod < 0.7 + tendency then
			target = roles[work]
		else if tod < 0.9 + tendency then
			target = roles[leisure]
		else target = roles[residence]

		if target == null then
			if roles[work] == null and roles[residence] == null then
				# Wait to leave with the space elevator
				for room in current_slot.station.rooms do
					if room.rule == turn.game.story.terminal then
						target = room
						break
					end
				end
				assert target != null
			else
				# Wander in a public place
				loop
					target = current_slot.station.rooms.rand
					if target.rule == turn.game.story.lobby or target.rule == turn.game.story.terminal then break
				end
			end
		end

		return target
	end

	redef fun to_s do return "{name} - position is ({x},{y}), job assigned: {current or else "None"}, current slot {current_slot}"
end

redef class Room

	# Roles with the assigned `Characters`
	var assigned_roles = new MultiHashMap[Role, Character]

	# Count of roles still available
	var available_roles: Map[Role, Int] is lazy do
		var map = new DefaultMap[Role, Int](0)
		for role in rule.roles do map[role] += 1
		return map
	end

	redef fun free_slots
	do
		super

		for role, characters in assigned_roles do for c in characters.to_a do
			c.leave_role role
		end
	end
end

abstract class Job
	super Turnable

	var title: Text
	var description: Text
	var cost: Map[Resource, Int]
	var revenue: Map[Resource, Int]

	var slot: Slot
	var employable: SequenceRead[CharacterRule]

	# Total duration of the job (in man seconds)
	var duration: Float

	# Progress of the job (in man seconds)
	var progress = 0.0

	# Priority of the job
	#
	# 1 - Normal
	# 2 - Higher
	# 3 - Urgent
	#
	# Starts at a normal priority
	var priority = 1

	# How much time will pass before priority is upgraded
	#
	# Defaults to 60 seconds
	var time_to_upgrade_priority = 60.0

	# How many people can work on this task
	#
	# Defautls to 1
	var max_people = 1

	var started = false

	init do
		var station = slot.station
		station.schedule(self)
	end

	redef fun to_s do return "{title} - priority {priority}, progress is {progress}/{duration}"

	fun is_finished: Bool do return false

	fun can_start: Bool do
		for k, v in cost do
			print "Checking resource {k} for {v} units"
			if not k.has(v) then return false
		end
		return true
	end

	fun start do
		for k, v in cost do k.inc v
		started = true
	end

	fun finish(s: Station) do end
end

# Jobs supposed to be handled by one or more workers in a timely manner.
class OneTimeJob
	super Job

	var paid = false

	redef fun is_finished do return progress >= duration

	redef fun update(turn) do
		if is_finished then return
		if not started and not can_start then return
		if not started then start
		progress += turn.dt
		if not paid and is_finished then
			for k, v in revenue do k.units += v
			paid = true
		end
	end
end

# Jobs repeatable infinitely, e.g. production of resources
#
# NOTE: Maintenance can be repeatable, although they might impact other jobs if not done
# quickly, therefore I suggest spawning them when needed as OneTimeJobs
class RepeatableJob
	super Job

	redef fun update(turn) do
		if not started and can_start then
			start
		else
			return
		end
		progress += turn.dt
		if progress >= duration then
			for k,v in revenue do k.units += v
			started = false
			progress = 0.0
		end
	end
end

class WalkJob
	super OneTimeJob

	init with_slot(slot: Slot) do
		var empty = once new HashMap[Resource, Int]
		var employable = once new Array[CharacterRule]
		init("Walking", "IDLE", empty, empty, slot, employable, 0.01)
	end
end

redef class Station
	var people = new HashSet[Character]

	var job_pool = new HashMap[Text, MinHeap[Job]]

	redef fun update(turn) do
		super
		for i in people do
			i.update(turn)
			#print i
		end
	end

	fun schedule(j: Job) do
		var pool = job_pool
		for i in j.employable do
			var str = i.name
			if not pool.has_key(str) then pool[str] = new MinHeap[Job](job_comparator)
			pool[str].add j
		end
	end

	fun pick_job(character: Character): nullable Job do
		var rule = character.rule
		var rule_name = rule.name
		var queue = job_pool.get_or_null(rule_name)
		if queue == null then return null
		var priority = -1
		var ch_slot = character.current_slot
		var job: nullable Job = null
		var job_plan: nullable PathPlan = null
		for i in queue do
			var njob_path = ch_slot.path_to(i.slot)
			if njob_path == null then continue
			if not i.started and not i.can_start then continue
			if job == null then
				job = i
				job_plan = njob_path
				priority = i.priority
				continue
			end
			if i.priority < priority then return job
			# NOTE: Unless I'm mistaken, job_plan cannot be null here, but adaptive typing
			# seems broken in this specific case, hence the `as(not null)` to avoid a warning here.
			if job_plan.as(not null).plan.length < njob_path.plan.length then continue
			job = i
			job_plan = njob_path
		end
		return job
	end
end

class JobComparator
	super Comparator

	redef type COMPARED: Job

	# Comparison is inverted to make the min heap a max heap
	redef fun compare(a, b) do
		if a.priority > b.priority then return -1
		if a.priority < b.priority then return 1
		return 0
	end
end

fun job_comparator: JobComparator do return once new JobComparator
