module room is serialize

import base

class Room
	super Ruled
	super Turnable

	redef type RULE: RoomRule

	# The associated station
	var station: Station

	# The slots occupied by the room
	var slots = new Array[Slot]

	# Free the slots occupied by the room
	fun free_slots
	do
		on_free
		for s in slots do s.free

		var empty_room = rule.story.empty_room
		if rule != empty_room and rule.structural then
			# Create empty rooms
			for s in slots do
				var r = empty_room.spawn_impl(s)
				r.on_spawn
			end
		end

		slots.clear
		station.rooms.remove self
	end

	# Can this room be freed without isolating a part of the tower?
	fun can_free: Bool
	do
		if rule.static then return false

		return rule != rule.story.empty_room or not holds_the_station_together
	end

	# Does this room holds the station together?
	# i.e. removing it would cut the station in two.
	#
	# Walk the graph to count all the rooms attached to the terminal
	# while ignoring this room.
	fun holds_the_station_together: Bool
	do
		var closed = new Set[Slot]
		var accessible_rooms = new Set[Room]
		var frontier = new Set[Slot]
		frontier.add station.slot(0, 0)

		while frontier.not_empty do
			var lot = frontier.first
			frontier.remove lot
			closed.add lot

			for n in lot.sides do
				var room = n.room
				if room != null and not closed.has(n) then
					accessible_rooms.add room
					if room == self or not room.rule.structural then
						closed.add n
					else frontier.add n
				end
			end
		end

		return accessible_rooms.length != station.rooms.length
	end

	redef fun to_s do return "{rule}x{slots.length}@{slots.first}"

	# Callback call after a room is spawned or updated
	fun on_spawn do end

	# Callback call before a room is freed
	fun on_free do end
end

class RoomAttempt
	# The room attempted
	var rule: RoomRule

	# The station considered
	var station: Station

	# The anchor bottom-left slot (player's cursor)
	var anchor: Slot

	# List of slots that will be build
	var slots = new Array[Slot]

	# All all mandatory checks OK?
	fun is_ok: Bool do return is_free and is_connected

	# Are all the slots free to build? (mandatory)
	var is_free = false

	# Is the room connected to a structural part? (mandatory)
	var is_connected = false

	# Effectively spawn a room.
	fun spawn: Room do
		var room = rule.spawn_impl(anchor)
		room.on_spawn
		return room
	end
end

class RoomRule
	super Rule

	var width = 1 is optional

	var height = 1 is optional

	var cost = 100 is optional

	var refund: Int = cost * 8 / 10 is optional

	var daily_cost = 0 is optional

	# Power supply (+) or demand (-) of an active room
	var power = 0.0 is optional

	# Power supply (+) or demand (-) of an each active workers/users
	var power_per_roles = 0.0 is optional

	# Oxygen supply of an active room
	var oxygen = 0.0 is optional

	# Oxygen supply of an each active workers/users
	var oxygen_per_roles = 0.0 is optional

	# Can the room be placed somewhere?
	fun try_spawn(bottom_left: Slot): RoomAttempt
	do
		var station = bottom_left.station
		var result = new RoomAttempt(self, station, bottom_left)

		result.is_free = true
		# Check that slots are free
		for i in [0..width[ do for j in [0..height[ do
			var slot = bottom_left.move(i, j)
			result.slots.add slot
			var room = slot.room
			if room != null then
				if room.rule == self or room.rule != story.empty_room then
					result.is_free = false
				end
			end
			result.is_connected = result.is_connected or slot.is_connected
		end

		return result
	end

	fun spawn_impl(bottom_left: Slot): Room
	do
		var station = bottom_left.station
		var room = new_room(station)
		for i in [0..width[ do for j in [0..height[ do
			var slot = bottom_left.move(i, j)

			var r = slot.room
			if r != null then r.free_slots

			slot.room = room
			room.slots.add slot
		end
		station.rooms.add room
		connect_walk(room)
		return room
	end

	# Create a new instance of `Room` of the type corresponding to `self`
	fun new_room(station: Station): Room do return new Room(self, station)

	# Make the slots of a room walkable
	# Called by `spawn_impl`
	#
	# By default in walkable rooms, only the floor is walkable and left and right doors are assumed
	fun connect_walk(room: Room)
	do
		if not walkable then return

		var bottom_left = room.slots.first

		# The bottom slots are walkable.
		# This includes the slot before and after the room (implicit doors)
		var prev = bottom_left.move(-1, 0)
		for i in [0..width+1[ do
			var slot = bottom_left.move(i, 0)
			prev.add_walk(slot)
			prev = slot
		end
	end

	# Is the room protected against being built and freed by the player? Defaults to `false`
	var static = false is optional

	# Does the room supports other nearby rooms? Defaults to `true`
	var structural = true is optional

	# Can the room have people inside? Defaults to `structural`
	#
	# This is used to have a floor (be default, see `connect_walk`) and show
	# unreachable warnings.
	var walkable: Bool = structural is optional

	# Roles available on this room as a flattened array
	#
	# Each instance/kind of `Role` may appear more than once.
	var roles = new Array[Role] is optional
end

# Station-lot. A discrete atomic area in the station.
class Slot
	# The associated station
	var station: Station

	# X coordinate
	var x: Int

	# Y coordinate
	var y: Int

	# The associated room, if any.
	var room: nullable Room = null

	# The associated room rule, if any.
	# This is a safe alias to `self.room.rule`
	fun rule: nullable RoomRule do
		var room = self.room
		if room == null then return null
		return room.rule
	end

	# The slot at x+dx,y+dy
	fun move(dx,dy: Int): Slot do return station.slot(x+dx, y+dy)
	# The slot above `self`
	fun up: Slot do return station.slot(x, y+1)
	# The slot bellow `self`
	fun down: Slot do return station.slot(x, y-1)
	# The slot at the left of `self`
	fun left: Slot do return station.slot(x-1, y)
	# The slot at the right of `self`
	fun right: Slot do return station.slot(x+1, y)

	# The 4 slots on each side
	var sides: Array[Slot] = [up, down, left, right] is lazy, noserialize

	# Is the slot next to an occupied slot with structural support?
	fun is_connected: Bool
	do
		for s in sides do
			var room = s.room
			if room != null and room.rule.structural then return true
		end
		return false
	end

	# List of walkable neighbors
	#
	# Used for path finding.
	var walk = new ArraySet[Slot]

	# Add a walk link between two slots
	fun add_walk(s: Slot)
	do
		if room == null or s.room == null then return
		if not rule.walkable or not s.rule.walkable then return
		walk.add s
		s.walk.add self
	end

	# Remove the room of the slot
	private fun free
	do
		room = null
		for n in walk do
			n.walk.remove self
		end
		walk.clear
	end

	redef fun to_s do return "({x},{y})"

	fun dump
	do
		printn "slot {self}: "
		var room = self.room
		if room == null then
			print "no room"
		else
			print "{room.rule.name} {room.slots.length}"
		end
		if not walk.is_empty then print "\twalk: {walk}"
	end
end

# Role of a character in relation to a room
class Role
	super Rule
end

redef class Station
	# All the rooms
	var rooms = new Array[Room]

	# All the slots
	var slots = new Array[Slot]

	# The slot at a given coordinate.
	#
	# It will be created and cached if needed.
	fun slot(x,y: Int): Slot
	do
		# TODO make it more efficient
		for s in slots do
			if s.x == x and s.y == y then return s
		end
		var s = new Slot(self, x, y)
		slots.add s
		return s
	end

	redef fun update(t) do
		super
		for r in rooms do r.update(t)
	end

	fun dump
	do
		var o = slot(0,0)
		var bb = new BB(o, o)
		for r in rooms do for s in r.slots do bb.update(s)

		for y in [bb.min.y..bb.max.y].reverse_iterator do
			for x in [bb.min.x..bb.max.x] do
				var s = self.slot(x,y)
				if s.room == null then
					printn "."
				else
					printn "X"
				end
			end
			print ""
		end
	end
end

redef class Game
	redef fun setup
	do
		story.terminal.try_spawn(station.slot(-1,0)).spawn
		story.cable.try_spawn(station.slot(0,-16)).spawn
		super
	end
end

# Used to compute the bonding box of a room
private class BB
	var max: Slot
	var min: Slot

	fun update(s: Slot)
	do
		if s.x > max.x or s.y > max.y then max = s.station.slot(s.x.max(max.x), s.y.max(max.y))
		if s.x < min.x or s.y < min.y then min = s.station.slot(s.x.min(min.x), s.y.min(min.y))
	end
end

class Lobby
	super RoomRule

	redef fun name do return "Lobby"

	redef fun width do return 2
	redef fun height do return 1
end

class Stair
	super RoomRule

	# Vertical speed
	var vspeed = 1.0 is optional

	redef fun connect_walk(room)
	do
		super
		for s in room.slots do
			if s.up.rule == self then s.up.add_walk(s)
			if s.down.rule == self then s.down.add_walk(s)
			s.left.add_walk(s)
			s.right.add_walk(s)
		end
	end
end

# Cable terminal platform
class Terminal
	super RoomRule

	redef fun name do return "Terminal"
	redef fun width do return 3
	redef fun static do return true
end

# Actual cable
class Cable
	super RoomRule

	redef fun name do return "Cable"
	redef fun height do return 16
	redef fun static do return true
	redef fun structural do return false
end

# An empty room to extend the station in space
class EmptyRoom
	super RoomRule

	redef fun name do return "Empty"

	redef fun walkable do return false
end

# An airlock isolates parts of a deck from depressurization
class Airlock
	super RoomRule

	redef fun name do return "Airlock"
end

redef class Story
	# ---
	# Roles
	var work = new Role(self, "Work")
	var residence = new Role(self, "Residence")
	var leisure = new Role(self, "Leisure")

	fun empty_room: EmptyRoom is abstract
	fun lobby: Lobby is abstract
	fun terminal: Terminal is abstract
	fun cable: Cable is abstract
end
