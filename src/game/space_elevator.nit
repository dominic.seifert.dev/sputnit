module space_elevator is serialize

import base

redef class Station
	var elevator = new SpaceElevator(self)

	redef fun update(turn)
	do
		super
		elevator.update turn
	end
end

class SpaceElevator
	super Turnable

	var station: Station

	# Time between departure and arrival (in prop of day_length)
	var period_out = 0.2

	# Time between arrival and departure (in prop of day_length)
	var period_in = 0.05

	# Time of the next event, arrival or departure
	var next_event_time: Float = period_out / 2.0 * 60.0

	# Is the next event at `next_event_time` an arrival? Otherwise it's a departure.
	var next_event_is_arrival = true

	redef fun update(turn)
	do
		super

		if turn.time > next_event_time then
			if next_event_is_arrival then
				on_arrival turn
				next_event_time += period_in * turn.game.day_length
			else
				on_departure turn
				next_event_time += period_out * turn.game.day_length
			end
			next_event_is_arrival = not next_event_is_arrival
		end
	end

	# Event on the car arrival to the station
	fun on_arrival(turn: Turn) do end

	# Event on the car departure from the station
	fun on_departure(turn: Turn) do end
end
