import sputnit
import android

redef class UICamera
	redef fun top_left
	do
		var s = super
		return new Point3d[Float](s.x, s.y - 64.0, s.z)
	end

	redef fun top_right
	do
		var s = super
		return new Point3d[Float](s.x, s.y - 64.0, s.z)
	end
end
