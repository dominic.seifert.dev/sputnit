NIT_DIR ?= ~/projects/nit/

bin/sputnit: $(shell nitls -M src/sputnit_linux.nit src/debug.nit) gen
	nitc -o $@ src/sputnit_linux.nit -m src/debug.nit -m src/gen/serial_linux.nit

bin/sputnit.apk: $(shell nitls -M src/sputnit_android.nit) gen
	nitc -o $@ src/sputnit_android.nit

gen: src/gen/characters_spritesheet.nit src/gen/serial_linux.nit

src/gen/serial_linux.nit: $(shell nitls -M src/sputnit_linux.nit)
	nitserial -o $@ src/sputnit_linux.nit

src/gen/characters_spritesheet.nit: art/characters_spritesheet.svg
	${NIT_DIR}/contrib/inkscape_tools/bin/svg_to_png_and_nit art/characters_spritesheet.svg -a assets/ -s src/gen/ -x 2.0 -g

check:
	nitunit src/tests src/game
